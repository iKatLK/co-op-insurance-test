import { Component, OnInit , Input, Output, EventEmitter  } from '@angular/core';
import { FormControl, Validators,FormGroup } from '@angular/forms';
import { AccountService } from "../../../services/account.service";
import { UserService } from "../../../services/user.service";
import { AuthService } from 'src/services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnInit {

  @Input() accountId : String;
  @Input() open : Boolean;
  @Output() modalClose = new EventEmitter();

  

  account: BankAccount;
  customers: User[];
  
  
  constructor( 
    private accountService : AccountService,
    private userService : UserService,
    private _flashMessagesService: FlashMessagesService

    ) { }



    accountForm = new FormGroup({
      name: new FormControl(''),
      branch: new FormControl(''),
      balance: new FormControl(''),
      ownerId: new FormControl(''),
    });

  ngOnInit() {
    this.getCustomers();
  }
  ngOnChanges(){

    this.getAccount(this.accountId);
    
  }

  getCustomers(){
    this.userService.getCustomers().subscribe((res) => {
      this.customers = res.result;
      }, (err) => {
        console.log(err);
      });
  }

  getAccount(id:String){

    if(id){
        this.accountService.getAccountByGuid(id).subscribe((res) => {
          this.account = res.result;
          this.setValues() ;
      }, (err) => {
        console.log(err);
      });
    }
    else {
      this.clearAccount();
      this.setValues() ;
    }
  }

  clearAccount(){
    this.accountId = "";
    this.account = {
      id: null,
      name :"",
      branch :"",
      balance  :0,
      owner : {
        id: null,
        firstName :"",
        lastName :"",
        email :"",
        role  :0,
        accounts :[]
      }
    }
    this.setValues() ;
  }

  addNewAccount(account:BankAccount){
    this.accountService.addAccount(account).subscribe((res) => {
      //console.log(res);
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
    }, (err) => {
      // console.log(err.error.title);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });


    
  }
  addUpdateAccount(account:BankAccount){
    this.accountService.updateAccount(account).subscribe((res) => {
      // console.log(res);
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
    }, (err) => {
      // console.log(err.error.title);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });
  }

  setValues() {
    this.accountForm
      .patchValue({
        name: this.account.name,
        branch: this.account.branch,
        balance: this.account.balance,
        ownerId: this.account.owner.id,
        owner: this.account.owner
    })
  }

  close(){
    this.open= false;
    this.modalClose.emit(this.open);
    this.clearAccount();
    this.setValues() ;
  }

  async onSubmit() {
    this.account = this.accountForm.value;
    if(this.accountId == "")
      await this.addNewAccount(this.account);
    else{
      this.account.id = this.accountId;
      await this.addUpdateAccount(this.account);
    }
  
    this.close();
  }

}
