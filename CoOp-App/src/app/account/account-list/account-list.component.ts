import { AccountService } from "../../../services/account.service";
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  openModal: Boolean = false;
  openModalView: Boolean = false;
  accounts: BankAccount[] = [];
  selectedAccountId: String;
  accountsBackup: BankAccount[] = [];
  previous: any = [];
  headElements = ['ID', 'Name', 'Branch', 'Balance', 'Owner', 'Actions'];
  searchText: string = '';

  firstItemIndex;
  lastItemIndex;
  constructor(
    private accountService: AccountService,
    private router: Router,
    private authService : AuthService,
    private _flashMessagesService: FlashMessagesService
  ) { }

  isManager : Boolean = this.authService.checkManager();

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ngOnInit() {

    this.getAccounts();

  }


  getAccounts() {
    
    this.accountService.getAccounts().subscribe((res) => {
      this.accounts = res.result;
      this.accountsBackup = this.accounts;
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'secondary-color', timeout: 5000 });
    }, (err) => {
      // console.log(err);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });
  }

  onNextPageClick(data: any) {

  }

  onPreviousPageClick(data: any) {

  }

  remove(i) {

    if (confirm("Are you sure do you want to delete ? ")) {
      this.accountService.removeAccountByGuid(this.accounts[i].id).subscribe(res => {
        //console.log(res);
        this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
        this.accounts.splice(i, 1);
      }, (err) => { 
        // console.log(err)
        this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
      });
    }
  }

  update(i) {
    this.selectedAccountId = this.accounts[i].id;
    this.openModal = true;
  }

  view(i) {
    this.selectedAccountId = this.accounts[i].id;
    this.openModalView = true;
  }

  newAccount() {
    this.selectedAccountId = "";
    this.openModal = true;
  }

  closeModal(state) {
    setTimeout(s=> this.getAccounts(),2000);
    this.openModal = state;
    this.selectedAccountId ='';
  }
  
  closeModalView(state) {
    this.openModalView = state;
  }


  searchItems() {
    this.accounts = this.accountsBackup.filter(
      a => a.name.toUpperCase().includes(this.searchText.toUpperCase())
        || a.branch.toUpperCase().includes(this.searchText.toUpperCase())
        || a.owner.firstName.toUpperCase().includes(this.searchText.toUpperCase())
        || a.owner.lastName.toUpperCase().includes(this.searchText.toUpperCase())

    );

  }
}
