import { Component, OnInit , Input, Output, EventEmitter  } from '@angular/core';
import { FormControl, Validators,FormGroup } from '@angular/forms';
import { AccountService } from "../../../services/account.service";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.scss']
})
export class AccountViewComponent implements OnInit {

  @Input() accountId : String;
  @Input() open : Boolean;
  @Output() modalClose = new EventEmitter();

  account: BankAccount = {
    id :'',
    name :'',
    branch : '',
    balance : 0,
    owner : {
      id :'',
      firstName :'',
      lastName : '',
      email : '',
      role : 0,
      accounts : []
    }
  };
  roles = ["Customer","Clerk","Manager"];

  constructor( 
    private accountService : AccountService
    ) { }

  ngOnInit() {
    
  }
  ngOnChanges(){

    this.getAccount(this.accountId);
    
  }

  getAccount(id:String){
        this.accountService.getAccountByGuid(id).subscribe((res) => {
          this.account = res.result;
      }, (err) => {
        console.log(err);
      });
  }

  close(){
    this.open= false;
    this.modalClose.emit(this.open);
  }

 

}
