import { Routes, CanActivate, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../services/auth-guard.service';
import { RoleGuardService as RoleGuard } from '../services/role-guard.service';
import { AccountListComponent } from './account/account-list/account-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';
import { LoginComponent } from './page/login/login.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './page/home/home.component';

export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent },
  { 
    path: 'account',
    component: AccountListComponent,
    canActivate: [AuthGuard] 
  },
  { 
    path: 'user', 
    component: UserListComponent, 
    canActivate: [RoleGuard], 
    data: { 
      expectedRole: 'Manager'
    }
  },
  { path: '**', component: PageNotFoundComponent  }
];

@NgModule({
    declarations: [],
    imports: [
      CommonModule,
      RouterModule.forRoot(ROUTES)
    ],
    exports:[RouterModule]
  })

export class AppRoutes { }