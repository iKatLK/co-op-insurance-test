import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes, ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { MainNavComponent } from './page/main-nav/main-nav.component';
import { FooterComponent } from './page/footer/footer.component';
import { AccountListComponent } from './account/account-list/account-list.component';
import { AccountFormComponent } from './account/account-form/account-form.component';
import { AccountViewComponent } from './account/account-view/account-view.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { LoginComponent } from './page/login/login.component';

import { UserService } from "../services/user.service";
import { AccountService } from "../services/account.service";
import { AuthService } from "../services/auth.service";
import { AuthGuardService as AuthGuard } from "../services/auth-guard.service";
import { RoleGuardService as RoleGuard } from "../services/role-guard.service";
import { HeaderComponent } from './page/header/header.component';
import { HomeComponent } from './page/home/home.component';
import { JwtHelperService, JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { FlashMessagesModule } from 'angular2-flash-messages';

const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: ()=> localStorage.getItem('access_token')
  }
};

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    FooterComponent,
    AccountListComponent,
    AccountFormComponent,
    AccountViewComponent,
    UserListComponent,
    UserViewComponent,
    UserFormComponent,
    LoginComponent,
    PageNotFoundComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(ROUTES),
    JwtModule.forRoot(JWT_Module_Options),
    FlashMessagesModule.forRoot(),
    AppRoutes,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    AccountService,
    AuthService,
    AuthGuard,
    RoleGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
