import { UserService } from "../../../services/user.service";
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  openModal: Boolean = false;
  openModalView: Boolean = false;
  users: User[] = [];
  selectedUserId: String;
  usersBackup: User[] = [];
  previous: any = [];
  headElements = ['ID', 'First', 'Last', 'Email', 'Role', 'Actions'];
  searchText: string = '';

  firstItemIndex;
  lastItemIndex;
  constructor(
    private userService: UserService,
    private router: Router,
    private _flashMessagesService: FlashMessagesService
  ) { }

  @HostListener('input') oninput() {
    this.searchItems();
  }

  ngOnInit() {

    this.getUsers();
    

  }


  getUsers() {
    
    this.userService.getUsers().subscribe((res) => {
      this.users = res.result;
      this.usersBackup = this.users;
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'secondary-color', timeout: 5000 });
    }, (err) => {
      // console.log(err);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });
  }

  getRole(roleId: Number) {
    return this.userService.getUserRole(roleId);
  }
  onNextPageClick(data: any) {

  }

  onPreviousPageClick(data: any) {

  }

  remove(i) {

    if (confirm("Are you sure do you want to delete ? ")) {
      this.userService.removeUserByGuid(this.users[i].id).subscribe(res => {
        //console.log(res);
        this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
        this.users.splice(i, 1);
      }, (err) => { 
        // console.log(err)
        this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
      });
    }
  }

  update(i) {
    this.selectedUserId = this.users[i].id;
    this.openModal = true;
  }

  view(i) {
    this.selectedUserId = this.users[i].id;
    this.openModalView = true;
  }

  newUser() {
    this.selectedUserId = "";
    this.openModal = true;
  }

  closeModal(state) {
    setTimeout(s=> this.getUsers(),2000);
    this.openModal = state;
  }
  closeModalView(state) {
    this.openModalView = state;
  }

  searchItems() {
    this.users = this.usersBackup.filter(
      u => u.firstName.toUpperCase().includes(this.searchText.toUpperCase())
        || u.lastName.toUpperCase().includes(this.searchText.toUpperCase())
        || u.email.toUpperCase().includes(this.searchText.toUpperCase())

    );

  }
}
