import { Component, OnInit , Input, Output, EventEmitter  } from '@angular/core';
import { FormControl, Validators,FormGroup } from '@angular/forms';
import { UserService } from "../../../services/user.service";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  @Input() userId : String;
  @Input() open : Boolean;
  @Output() modalClose = new EventEmitter();

  user: User = {
    id :'',
    firstName :'',
    lastName : '',
    email : '',
    role : 0,
    accounts : []
  };
  roles = ["Customer","Clerk","Manager"];

  constructor( 
    private userService : UserService
    ) { }

  ngOnInit() {
    
  }
  ngOnChanges(){

    this.getUser(this.userId);
    
  }

  getUser(id:String){
        this.userService.getUserByGuid(id).subscribe((res) => {
          this.user = res.result;
      }, (err) => {
        console.log(err);
      });
  }

  close(){
    this.open= false;
    this.modalClose.emit(this.open);
  }

 

}
