import { Component, OnInit , Input, Output, EventEmitter  } from '@angular/core';
import { FormControl, Validators,FormGroup } from '@angular/forms';
import { UserService } from "../../../services/user.service";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  @Input() userId : String;
  @Input() open : Boolean;
  @Output() modalClose = new EventEmitter();

  

  user: User;
  roles = ["Customer","Clerk","Manager"];
  constructor( 
    private userService : UserService,
    private _flashMessagesService: FlashMessagesService
    ) { }

    userForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      role: new FormControl(''),
    });

  ngOnInit() {
    
  }
  ngOnChanges(){

    
    this.getUser(this.userId);
    
  }

  getUser(id:String){
    if(id){
        this.userService.getUserByGuid(id).subscribe((res) => {
          this.user = res.result;
          this.setValues() ;
      }, (err) => {
        console.log(err);
      });
    }
    else {
      this.user = {
        id: null,
        firstName :"",
        lastName :"",
        email :"",
        role  :0,
        accounts :[]
      }
      this.setValues() ;
    }
 

  }

  addNewUser(user:User){
    this.userService.addUser(user).subscribe((res) => {
      // console.log(res);
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
    }, (err) => {
      // console.log(err.error.title);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });


    
  }
  addUpdateUser(user:User){
    this.userService.updateUser(user).subscribe((res) => {
      //console.log(res);
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });
    }, (err) => {
      //console.log(err);
      this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });


    
  }

  setValues() {
    this.userForm
      .patchValue({
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email,
        role: this.user.role,
    })
  }

  close(){
    this.open= false;
    this.modalClose.emit(this.open);
  }

  async onSubmit() {
    this.user = this.userForm.value;
    if(this.userId == "")
      await this.addNewUser(this.user);
    else{
      this.user.id = this.userId;
      await this.addUpdateUser(this.user);
    }
      
    this.close();
  }

}
