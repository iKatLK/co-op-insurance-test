import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
import { AuthService } from "../../../services/auth.service";
import { Router, CanActivate } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor( 
    private authService : AuthService,
    public router: Router,
    private _flashMessagesService: FlashMessagesService
    ) { }

    token : String ;
    loginForm = new FormGroup({

      email: new FormControl(''),
      password: new FormControl(''),

    });

  ngOnInit() {

  }

  onSubmit() {

    this.authService.Login(this.loginForm.value.email,this.loginForm.value.password).subscribe((res) => {
      
      this.token = res.result;
      localStorage.setItem('token',this.token.toString());
      window.location.replace(window.location.origin);
      this._flashMessagesService.show(res.title.toString(), { cssClass: 'success-color', timeout: 5000 });

      }, (err) => {
        // console.log(err.error.title);
        this._flashMessagesService.show(err.error.title.toString(), { cssClass: 'danger-color-dark', timeout: 5000 });
    });
    }
}
