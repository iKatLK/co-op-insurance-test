import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth.service";
import { Router, CanActivate } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  constructor( 
    private authService : AuthService,
    public router: Router,
    private _flashMessagesService: FlashMessagesService
    ) { }

  isLogged : Boolean;

  ngOnInit() {
    this.isLogged =  this.authService.isAuthenticated();
  }

  ngOnChange(){
    
  }

  logOut(){
    this.authService.lotOut();
    window.location.replace(window.location.origin + '/login');
    this._flashMessagesService.show("Logout Success !", { cssClass: 'success-color', timeout: 5000 });
    
  }

}
