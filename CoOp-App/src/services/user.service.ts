import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../configurations/config';
import { AuthService } from "./auth.service";
@Injectable({
  providedIn: 'root'
})
export class UserService {

  

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
    ) { }

    private  context : string = "user";
   

    private httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${this.authService.getToken()}`
      })
    };


  getUsers(): Observable<Result<User[]>> {
    return this.httpClient.get<Result<User[]>>(`${config.SERVER}/${this.context}`,this.httpOptions);
  }
  getCustomers(): Observable<Result<User[]>> {
    return this.httpClient.get<Result<User[]>>(`${config.SERVER}/${this.context}/customers`,this.httpOptions);
  }

  getUserByGuid(guid : String): Observable<Result<User>> {
    return this.httpClient.get<Result<User>>(`${config.SERVER}/${this.context}/${guid}`,this.httpOptions);
  }

  addUser(user : User): Observable<Result<User>> {
    return this.httpClient.post<Result<User>>(`${config.SERVER}/${this.context}`,user,this.httpOptions);
  }

  updateUser(user : User): Observable<Result<User>> {
    return this.httpClient.put<Result<User>>(`${config.SERVER}/${this.context}/${user.id}`,user,this.httpOptions);
  }

  removeUserByGuid(guid : String): Observable<Result<Boolean>> {
    return this.httpClient.delete<Result<Boolean>>(`${config.SERVER}/${this.context}/${guid}`,this.httpOptions);
  }
  
  getUserRole(roleId:Number) : String {
    return (roleId==2)? "Manager" : (roleId==1)? "Clerk" : "Customer";
  }
}

