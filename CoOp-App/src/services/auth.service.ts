import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../configurations/config';
import decode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  context : String = "user";

  constructor(
    private jwtHelper: JwtHelperService,
    private httpClient: HttpClient
    ) { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  public Login(email:String, password:String): Observable<Result<String>> {
    return this.httpClient.post<Result<String>>(`${config.SERVER}/${this.context}/login`,{email:email,password:password});
  }

  public lotOut() {
    localStorage.removeItem('token');
  }
  
  public getToken = ()=> (localStorage.getItem('token')!=undefined)? localStorage.getItem('token') :'';

  public checkManager = ()=> (localStorage.getItem('token')!=undefined && decode(localStorage.getItem('token')).AuthorizeLevel==2)? true :false;

  


}
