import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';
import decode from 'jwt-decode';

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {

    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;

    const token = localStorage.getItem('token');
    
    // decode the token to get its payload
    
    const tokenPayload = (token!=undefined)? decode(token) : '';

    if (
      !this.auth.isAuthenticated() ||  this.getRole(tokenPayload.AuthorizeLevel) !== expectedRole
    ) {
      this.router.navigate(['error']);
      return false;
    }
    return true;
  }

  getRole = (role)=> (role==2)? 'Manager' : (role==1)? 'Clerk' : 'Customer';

}