import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { config } from '../configurations/config';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
    ) { }

    private  context : string = "account";
   

    private httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${this.authService.getToken()}`
      })
    };

  getAccounts(): Observable<Result<BankAccount[]>> {
    return this.httpClient.get<Result<BankAccount[]>>(`${config.SERVER}/${this.context}`,this.httpOptions);
  }

  getAccountByGuid(guid : String): Observable<Result<BankAccount>> {
    return this.httpClient.get<Result<BankAccount>>(`${config.SERVER}/${this.context}/${guid}`,this.httpOptions);
  }

  addAccount(account : BankAccount): Observable<Result<BankAccount>> {
    return this.httpClient.post<Result<BankAccount>>(`${config.SERVER}/${this.context}`,account,this.httpOptions);
  }

  updateAccount(account : BankAccount): Observable<Result<BankAccount>> {
    return this.httpClient.put<Result<BankAccount>>(`${config.SERVER}/${this.context}/${account.id}`,account,this.httpOptions);
  }

  removeAccountByGuid(guid : String): Observable<Result<Boolean>> {
    return this.httpClient.delete<Result<Boolean>>(`${config.SERVER}/${this.context}/${guid}`,this.httpOptions);
  }
  
}

