interface User{
    id : String;
    firstName : String;
    lastName : String;
    email : String;
    role : Number;
    accounts : BankAccount[];
}