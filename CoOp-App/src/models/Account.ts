interface BankAccount{

    id : String;
    name : String;
    branch : String;
    balance : Number;
    owner : User;

}