# Co-op Insurance test

CO-OP Insurance Test Project

Instructions
Required ASP.NET Core 2.2 SDK & Node v8.0 or 10.0 & npm installed.

Use git clone to clone the repository.
~ git clone https://gitlab.com/iKatLK/co-op-insurance-test.git
~ cd co-op-insurance-test

Run Backend,
~ cd CoOpApi
~ dotnet run 

Run Backend,
~ cd ..
~ cd CoOp-App
~ npm install
~ ng serve -o

Login Details.

Manage-Role
Email - user@admin.com
Password - 12345678

Clerk-Role
Email - clerk@user.com
Password - 12345678
