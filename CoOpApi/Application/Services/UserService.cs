using System.Collections.Generic;
using System.Linq;
using CoOp.Domain.Models;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using CoOp.Domain;
using CoOp.Domain.Services;
using System.Threading.Tasks;
using AutoMapper;

namespace CoOp.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitofwork;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitofwork, IConfiguration config, IMapper mapper)
        {
            _unitofwork = unitofwork;
            _config = config;
            _mapper = mapper;

        }
        private UserView GetView(User user) => _mapper.Map<UserView>(user);
        private IEnumerable<UserView> GetView(IEnumerable<User> users) => _mapper.Map<IEnumerable<UserView>>(users);

        public async Task<IEnumerable<UserView>> GetAllUsers()
        {
            return GetView(await _unitofwork.UserRepository.FindAllUsers());
        }

        public async Task<IEnumerable<UserView>> GetAllCustomers()
        {
            return GetView(await _unitofwork.UserRepository.FindByCondition(u=>u.Role==UserRole.Customer));
        }




        public async Task<UserView> NewUser(User user)
        {

            if(user.Role != UserRole.Customer)
                user.Accounts = null;

            user.Password = HashPassword(user.Password);

            await _unitofwork.UserRepository.Create(user);

            var userByEmail = await _unitofwork.UserRepository.FindByCondition(u=>u.Email==user.Email);
            var registeredUser = userByEmail.FirstOrDefault();

            return GetView(registeredUser);
        }


        public async Task<User> GetUserByID(string guid)
        {
            return (await _unitofwork.UserRepository.FindByConditionUsers(u => u.Id== new Guid(guid))).FirstOrDefault();
        }
        private string CalculateMD5Hash(string input)

        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();

        }

        public async Task<bool> DeleteUserByID(string guid)
        {
            var user =  (await _unitofwork.UserRepository.FindByCondition(u=>u.Id.ToString()==guid)).FirstOrDefault();
            await _unitofwork.UserRepository.Delete(user);
            
            return true; // To be impliment
        }

        public async Task<UserView> UpdateUserByID(string guid, UserView data)
        {
            var user = await GetUserByID(guid);
            if (data.FirstName != null) user.FirstName = data.FirstName;
            if (data.LastName != null) user.LastName = data.LastName;
            if (data.Email != null) user.Email = data.Email;
            if (data.Role != null) user.Role = data.Role;

            await _unitofwork.UserRepository.Update(user);
            return  GetView(await GetUserByID(guid));
        }

        public async Task<string> Login(Login login)
        {

            var user = (await _unitofwork.UserRepository.FindByConditionUsers(

                    u => u.Email.Equals(login.Email, StringComparison.InvariantCultureIgnoreCase)
                            && u.Password.Equals(HashPassword(login.Password))

                )).FirstOrDefault();

            if (user == null)
            {
                return "";
            }


            var claims = new[] {

                new Claim("FirstName", user.FirstName),
                new Claim("LastName", user.LastName),
                //new Claim("AuthorizeLevel", user.Role.ToString()),
                new Claim("AuthorizeLevel", ((byte)user.Role).ToString()),
                new Claim("ID", user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, user.Role.ToString())
                
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        private string HashPassword(string password)
        {

            byte[] salt = new byte[128 / 8];



            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                                password: password,
                                salt: salt,
                                prf: KeyDerivationPrf.HMACSHA1,
                                iterationCount: 10000,
                                numBytesRequested: 256 / 8));


        }

    }


}