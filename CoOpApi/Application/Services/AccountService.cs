using System.Collections.Generic;
using System.Linq;
using CoOp.Domain.Models;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using CoOp.Domain;
using CoOp.Domain.Services;
using System.Threading.Tasks;
using AutoMapper;

namespace CoOp.Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitofwork;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public AccountService(IUnitOfWork unitofwork, IConfiguration config, IMapper mapper)
        {
            _unitofwork = unitofwork;
            _config = config;
            _mapper = mapper;

        }
        private AccountView GetView(Account account) => _mapper.Map<AccountView>(account);
        private IEnumerable<AccountView> GetView(IEnumerable<Account> accounts) => _mapper.Map<IEnumerable<AccountView>>(accounts);

        public async Task<IEnumerable<AccountView>> GetAllAccounts()
        {
            return GetView(await _unitofwork.AccountRepository.FindAllAccounts());
        }



        public async Task<AccountView> NewAccount(Account account)
        {

            await _unitofwork.AccountRepository.Create(account);

            var enteredAccount = await _unitofwork.AccountRepository.FindByCondition(a=>a.OwnerID==account.OwnerID);
            var registeredAccount = enteredAccount.LastOrDefault();

            return GetView(registeredAccount);
        }


        public async Task<Account> GetAccountByID(string guid)
        {
            return (await _unitofwork.AccountRepository.FindByConditionAccounts(u => u.Id.ToString() == guid)).FirstOrDefault();
        }
        private string CalculateMD5Hash(string input)

        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();

        }

        public async Task<bool> DeleteAccountByID(string guid)
        {
            var account =  (await _unitofwork.AccountRepository.FindByCondition(u=>u.Id.ToString()==guid)).FirstOrDefault();
            await _unitofwork.AccountRepository.Delete(account);
            
            return true; // To be impliment
        }

        public async Task<AccountView> UpdateAccountByID(string guid, Account data)
        {
            var account = await GetAccountByID(guid);
            if (data.Name != null) account.Name = data.Name;
            if (data.Balance != null) account.Balance = data.Balance;
            if (data.Branch != null) account.Branch = data.Branch;

            await _unitofwork.AccountRepository.Update(account);
            return  GetView(await GetAccountByID(guid));
        }

        public async Task<IEnumerable<Account>> GetAccountsByUserID(string guid)
        {
            return await _unitofwork.AccountRepository.FindByConditionAccounts(a => a.Owner.Id.ToString() == guid);
        }


    }


}