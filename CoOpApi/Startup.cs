﻿using System.Text;
using CoOp.Application.Services;
using CoOp.Domain;
using CoOp.Domain.Repositories;
using CoOp.Domain.Services;
using CoOp.Persistence;
using CoOp.Persistence.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using AutoMapper;
using CoOp.Mappings;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc.Authorization;
using System;
using CoOp.Domain.Models;

namespace CoOpApi
{
    public class Startup
    {
        // Configurations reffers appsetting.json
        public IConfiguration Configuration { get; }

        //Hosting enviroments
        public IHostingEnvironment env { get; }

        //Startup Constructor
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            env = hostingEnvironment;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Using different databases for diffrent enviroments.
            if (env.IsDevelopment())
            {
                var connection = Configuration["sqlliteconnection:connectionString"];
                services.AddDbContext<CoOpContext>(options => options.UseSqlite(connection));
            }
            else if (env.IsProduction())
            {
                var connection = Configuration["mssqlconnection:connectionString"];
                services.AddDbContext<CoOpContext>(options => options.UseSqlServer(connection));
            }

            // Swagger API Document added
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "CO-OP Insurance Test", Version = "v1" }));


            // Repositories
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();

            //Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAccountService, AccountService>();

            //Other instances
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddAutoMapper();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });



            services.AddMvc(options =>
                        {   
                            //If you want to disable authenication on delelopment enviroment

                            // if (env.IsDevelopment())
                            //     options.Filters.Add(new AllowAnonymousFilter());
                        })

                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)

                    //Jsonlooping restricted
                    .AddJsonOptions(options =>
                        {
                            options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                        });

                    }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "CO-OP Insurance Test API V1.0");
                });
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            
            }
            else if (env.IsProduction())
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
