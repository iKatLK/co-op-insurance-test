
using CoOp.Domain.Models;
using AutoMapper;

namespace CoOp.Mappings
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<User, UserView>();
        }
    }
}