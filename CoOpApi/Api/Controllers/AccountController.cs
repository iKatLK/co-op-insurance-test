using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CoOp.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using CoOp.Domain.Services;
using System.Threading.Tasks;
using System;

namespace CoOp.API.Controllers
{

    [Route("api/[controller]")]
    // [Authorize(Policy = "ManagerPolicy")]
    [Authorize(Roles = "Manager,Clerk")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountservice;

        public AccountController(IAccountService accountservice)

        {
            _accountservice = accountservice;

        }
        [HttpPost(Name = "Create New Account")]
        public async Task<ActionResult> Create(Account account)
        {
            var result = await _accountservice.NewAccount(account);

            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Account Adding Failed !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Account Successfully Added !"
                });
            }


        }


        [HttpGet(Name = "GetAllAccounts")]
        public async Task<ActionResult> GetAll()
        {

            var result = await _accountservice.GetAllAccounts();


            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Nothing to Found !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Accounts List Loaded Sucessfully !"
                });
            }

        }

        [HttpGet("{guid}", Name = "GetAccountByID")]
        public async Task<ActionResult> GetAccountByID(string guid)
        {
            var result = await _accountservice.GetAccountByID(guid);
            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Requested Account not found !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Account Loaded Sucessfully !"
                });
            }


        }

        [Authorize(Roles = "Manager")]
        [HttpPut("{guid}", Name = "UpdateAccountByID")]
        public async Task<ActionResult> Create(string guid, [FromBody] Account account)
        {
            var result = await _accountservice.UpdateAccountByID(guid, account);

            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Updating Account not complete !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Account Updated Sucessfully !"
                });
            }
        }

        [Authorize(Roles = "Manager")]
        [HttpDelete("{guid}", Name = "DeleteAccountByID")]
        public async Task<ActionResult> DeleteAccountByID(string guid)
        {
            var result = await _accountservice.DeleteAccountByID(guid);
            if (result != true)
            {
                return BadRequest(new
                {
                    title = "Deleting Account not complete !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = $"{guid} Account Successfully Deleted!"
                });
            }
        
        }

    }
}