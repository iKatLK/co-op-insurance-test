using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CoOp.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using CoOp.Domain.Services;
using System.Threading.Tasks;
using System;

namespace CoOp.API.Controllers
{

    [Route("api/[controller]")]

    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userservice;

        public UserController(IUserService userservice)

        {
            _userservice = userservice;

        }

        [Authorize(Roles = "Manager")]
        [HttpPost(Name = "Create New User")]
        public async Task<ActionResult> Create(User user)
        {
            var result = await _userservice.NewUser(user);

            if (result == null)
            {
                return BadRequest(new
                {
                    title = "User Adding Failed !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "User Successfully Added !"
                });
            }


        }

        [Authorize(Roles = "Manager")]
        [HttpGet(Name = "GetAllUsers")]
        public async Task<ActionResult> GetAll()
        {

            var result = await _userservice.GetAllUsers();


            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Nothing to Found !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Users List Loaded Sucessfully !"
                });
            }

        }

        [Authorize(Roles = "Manager,Clerk")]
        [HttpGet("Customers",Name = "GetAllCustomers")]
        public async Task<ActionResult> GetAllCustomers()
        {

            var result = await _userservice.GetAllCustomers();


            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Nothing to Found !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "Customers List Loaded Sucessfully !"
                });
            }

        }

        [Authorize(Roles = "Manager")]
        [HttpGet("{guid}", Name = "GetUserByID")]
        public async Task<ActionResult> GetUserByID(string guid)
        {
            var result = await _userservice.GetUserByID(guid);
            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Requested User not found !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "User Loaded Sucessfully !"
                });
            }


        }


        [Authorize(Roles = "Manager")]
        [HttpPut("{guid}", Name = "UpdateUserByID")]
        public async Task<ActionResult> Create(string guid, [FromBody] UserView user)
        {

            var result = await _userservice.UpdateUserByID(guid, user);

            if (result == null)
            {
                return BadRequest(new
                {
                    title = "Updating User not complete !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = "User Updated Sucessfully !"
                });
            }
        }

        [Authorize(Roles = "Manager")]
        [HttpDelete("{guid}", Name = "DeleteUserByID")]
        public async Task<ActionResult> DeleteUserByID(string guid)
        {
            var result = await _userservice.DeleteUserByID(guid);
            if (result != true)
            {
                return BadRequest(new
                {
                    title = "Deleting User not complete !"
                });
            }
            else
            {

                return Ok(new
                {
                    result = result,
                    title = $"{guid} User Successfully Deleted!"
                });
            }
        
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult> Login(Login login)
        {

            var token = await _userservice.Login(login);
            if (token == "")
            {
                return Unauthorized(new
                {
                    title = "Login Failed"
                });
            }

            return Ok(new
                {
                    result = token,
                    title = $"You are successfully logged in."
                });
        }

    }
}