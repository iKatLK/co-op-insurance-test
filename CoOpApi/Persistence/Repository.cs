using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoOp.Domain;
using Microsoft.EntityFrameworkCore;

namespace CoOp.Persistence.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected CoOpContext _context { get; set; }
 
        public Repository(CoOpContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<T>> FindAll() { 

            return await this._context.Set<T>().ToListAsync();

        }

        public async Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return await this._context.Set<T>().Where(expression).ToListAsync();
        }
    
        public async Task Create(T entity)
        {
            this._context.Set<T>().Add(entity);
            await Save();
        }
 
        public async Task Update(T entity)
        {
            this._context.Set<T>().Update(entity);
            await Save();
        }
 
        public async Task Delete(T entity)
        {
            this._context.Set<T>().Remove(entity);
            await Save();
        }
 
        private async Task Save()
        {
             await this._context.SaveChangesAsync();
        }
    }
}