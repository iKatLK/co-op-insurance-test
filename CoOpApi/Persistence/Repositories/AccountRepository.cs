using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoOp.Domain.Models;
using CoOp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CoOp.Persistence.Repositories
{

    public class AccountRepository : Repository<Account>,IAccountRepository
    {
        
        public AccountRepository(CoOpContext context) : base(context)

        {
           
        }

        public async Task<IEnumerable<Account>> FindAllAccounts()
        {
            return await this._context.Accounts
                   .Include(u => u.Owner)
                   .ToListAsync();
        }
        public async Task<IEnumerable<Account>> FindByConditionAccounts(Expression<Func<Account, bool>> expression)
        {
            return await this._context.Accounts
                    .Where(expression)
                   .Include(u => u.Owner)
                   .ToListAsync();
        }

        
  
    }
}