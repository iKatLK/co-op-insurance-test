using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoOp.Domain.Models;
using CoOp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CoOp.Persistence.Repositories
{

    public class UserRepository : Repository<User>,IUserRepository
    {
        
        public UserRepository(CoOpContext context) : base(context)

        {
           
        }

        public async Task<IEnumerable<User>> FindAllUsers()
        {
            return await this._context.Users
                    .Include(u => u.Accounts)
                   .ToListAsync();
        }
        public async Task<IEnumerable<User>> FindByConditionUsers(Expression<Func<User, bool>> expression)
        {
            return await this._context.Users
                    .Where(expression)
                    .Include(u => u.Accounts)
                   .ToListAsync();
        }

        
  
    }
}