using CoOp.Domain;
using CoOp.Domain.Repositories;

namespace CoOp.Persistence
{
    public class UnitOfWork :IUnitOfWork
    {
        private CoOpContext _context;
        public IUserRepository UserRepository { get; }
        public IAccountRepository AccountRepository { get; }


        public UnitOfWork(

            CoOpContext context, 
            IUserRepository userRepository,
            IAccountRepository accountRepository
            
            )


        {
            _context = context;
            UserRepository = userRepository;
            AccountRepository = accountRepository;


        }


    }
}