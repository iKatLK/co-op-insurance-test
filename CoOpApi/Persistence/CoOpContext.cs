using Microsoft.EntityFrameworkCore;
using CoOp.Domain.Models;

namespace CoOp.Persistence
{
    public class CoOpContext : DbContext
    {
        public CoOpContext(DbContextOptions<CoOpContext> options)
            : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<Account>()
                .HasOne(a => a.Owner)
                .WithMany(u => u.Accounts)
                .HasForeignKey(a => a.OwnerID);

            
        }

    }
}