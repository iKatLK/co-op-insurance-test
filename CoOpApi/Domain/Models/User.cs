using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CoOp.Domain.Models
{

    public class User
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }                                                                    
        [Required]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]             //// This regex allows characters spaces dashes & underscores only
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]             //// This regex allows characters spaces dashes & underscores only
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]                                            //// This email annotation tag allowed email formats only.
        public string Email { get; set; }
        [Required]
        [StringLength(50,MinimumLength = 8,ErrorMessage = "Password must contain minimum 8 characters.")]
        public string Password { get; set; }
        public UserRole? Role { get; set; }
        public ICollection<Account> Accounts { get; set; }

    }

    
    public class UserView
    {   

        public Guid Id { get; set; }
        public string FirstName { get;set; }
        public string LastName { get; set; }                                      
        public string Email { get; set; }
        public UserRole? Role { get; set; }
        public ICollection<AccountView> Accounts { get; set; }

    }
    public class Login
    {   

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required]
        [StringLength(50,MinimumLength = 8,ErrorMessage = "Password must contain minimum 8 characters.")]
        public string Password { get; set; }
    }


    public enum UserRole : Byte
    {
        Customer = 0,
        Clerk = 1,
        Manager = 2
    }
}