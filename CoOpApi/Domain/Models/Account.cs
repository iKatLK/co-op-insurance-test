using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace CoOp.Domain.Models
{

    public class Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; } 
        [Required]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]             //// This regex allows characters spaces dashes & underscores only
        public string Name { get; set; }
        [Required]
        [Range(0.0, Double.MaxValue,ErrorMessage = "Balance should be positive")]
        public double? Balance { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z]+[ a-zA-Z-_]*$", ErrorMessage = "Use Characters only")]             //// This regex allows characters spaces dashes & underscores only
        public string Branch { get; set; }
        public Guid OwnerID { get; set; }
        public User Owner { get; set; }
    }

    public class AccountView
    {
        public Guid Id { get; set; } 
        public string Name { get; set; }
        public double? Balance { get; set; }
        public string Branch { get; set; }
        public UserView Owner { get; set; }
    } 


}