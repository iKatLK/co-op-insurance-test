using CoOp.Domain.Repositories;

namespace CoOp.Domain
{
    public interface IUnitOfWork
    {
        
        IUserRepository UserRepository { get; }
        IAccountRepository AccountRepository { get; }

    }
}