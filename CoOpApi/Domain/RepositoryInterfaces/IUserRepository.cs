
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoOp.Domain.Models;


namespace CoOp.Domain.Repositories{

    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> FindAllUsers();
        Task<IEnumerable<User>> FindByConditionUsers(Expression<Func<User, bool>> expression);
    }
    
}