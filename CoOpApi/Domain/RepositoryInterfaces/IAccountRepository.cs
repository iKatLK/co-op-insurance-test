
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CoOp.Domain.Models;


namespace CoOp.Domain.Repositories{

    public interface IAccountRepository : IRepository<Account>
    {
        Task<IEnumerable<Account>> FindAllAccounts();
        Task<IEnumerable<Account>> FindByConditionAccounts(Expression<Func<Account, bool>> expression);
    }
    
}