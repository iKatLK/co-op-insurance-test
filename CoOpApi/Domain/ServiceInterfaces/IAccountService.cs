using System.Collections.Generic;
using System.Threading.Tasks;
using CoOp.Domain.Models;

namespace CoOp.Domain.Services
{
    public interface IAccountService
    {
        Task<IEnumerable<AccountView>> GetAllAccounts();
        Task<Account> GetAccountByID(string guid);
        Task<IEnumerable<Account>> GetAccountsByUserID(string guid);
        Task<AccountView> NewAccount(Account account);
        Task<AccountView> UpdateAccountByID(string guid, Account account);
        Task<bool> DeleteAccountByID(string guid);

    }
}