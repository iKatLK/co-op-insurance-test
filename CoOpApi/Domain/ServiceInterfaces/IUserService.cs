using System.Collections.Generic;
using System.Threading.Tasks;
using CoOp.Domain.Models;

namespace CoOp.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserView>> GetAllUsers();
        Task<IEnumerable<UserView>> GetAllCustomers();
        Task<User> GetUserByID(string guid);
        Task<UserView> NewUser(User user);
        Task<string> Login(Login login);
        Task<UserView> UpdateUserByID(string guid, UserView user);
        Task<bool> DeleteUserByID(string guid);

    }
}