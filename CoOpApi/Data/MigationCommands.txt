$env:ASPNETCORE_ENVIRONMENT='Development'
dotnet ef migrations add mSqlLite --context CoOp.Persistence.CoOpContext --output-dir Data/Migrations/SqlLiteMigrations
dotnet ef database update mSqlLite